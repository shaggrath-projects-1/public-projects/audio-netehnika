#include "LiquidLiteMenu.h"
#include "LiquidCrystal_I2C.h"
#include "AmplifireControll.h"
#include "GyverEncoder.h"
#include "Power.h"

uint8_t rFocus[8] = {
	0b00100,
	0b00110,
	0b00101,
	0b00100,
	0b00100,
	0b01100,
	0b10100,
	0b11000
};

#ifdef __arm__
// should use uinstd.h to define sbrk but Due causes a conflict
extern "C" char* sbrk(int incr);
#else  // __ARM__
extern char *__brkval;
#endif  // __arm__
  
int freeMemory() {
  char top;
#ifdef __arm__
  return &top - reinterpret_cast<char*>(sbrk(0));
#elif defined(CORE_TEENSY) || (ARDUINO > 103 && ARDUINO != 151)
  return &top - __brkval;
#else  // __arm__
  return __brkval ? &top - __brkval : &top - __malloc_heap_start;
#endif  // __arm__
}

LiquidLiteMenu::LiquidLiteMenu() {};

void LiquidLiteMenu::init(byte button2, byte button3, byte switchMode1, byte switchMode2, byte irData, byte encoderSW, byte encoderDT, byte encoderCLK, LiquidCrystal_I2C* lcd, AmplifireControll* ac, Power* power) {
    Encoder encoder(encoderCLK, encoderDT, encoderSW, TYPE2);
    encoder.setTickMode(AUTO);
    _encoder = encoder;
    _lcd = lcd;
    _ac = ac;
    _power = power;

    _button2 = button2;
    _button3 = button3;
    _switchMode1 = switchMode1;
    _switchMode2 = switchMode2;
    _irData = irData;
    _menuPointer = 0;
    _menuMode = 0;
    _updteDisplay = 1;
    _menuSize = 10;
    _menuTimeOut = 0;
    _dynamicScreenTimer = 0;
    _dynamicScreen = 0;
}

void LiquidLiteMenu::tick() {
    auto focus0 = _menuMode == 1 ? "#" : " ";
    auto focus1 = _menuMode == 0 ? "#" : " ";    
    char volume0[18];
    char volume1[18];
    sprintf(volume0, "%sVolume:", focus0);
    sprintf(volume1, "%s%d (dB) %d%%", focus1, _ac->getVolumeData().humanVal, _ac->getVolumeData().percentVal);
    char bass0[18];
    char bass1[18];
    sprintf(bass0, "%sBass:", focus0);
    sprintf(bass1, "%s%d (dB) %d%%", focus1, _ac->getBassData().humanVal, _ac->getBassData().percentVal);
    char treble0[18];
    char treble1[18];
    sprintf(treble0, "%sTreble: ", focus0);
    sprintf(treble1, "%s%d (dB) %d%%", focus1, _ac->getTrebData().humanVal, _ac->getTrebData().percentVal);
    char source0[18];
    char source1[18];
    sprintf(source0, "%sSource: ", focus0);
    sprintf(source1, "%schannel %s", focus1, _ac->getSourceData().humanVal == 1 ? "A" : "B");
    char mode0[18];
    char mode1[18];
    sprintf(mode0, "%sMode: ", focus0);
    sprintf(mode1, "%s%s", focus1, _ac->getModeData().humanVal == 1 ? "Quad" : "Power Stereo" );

    char voltageV[5];
    char currentPV[5];
    char currentNV[5];
    dtostrf( _power->getVoltageValue(), 3, 2, voltageV);
    dtostrf( _power->getCurrentPositiveValue(), 3, 2, currentPV);
    dtostrf( _power->getCurrentNegativeValue(), 3, 2, currentNV);

    char voltage0[18];
    char voltage1[18];
    sprintf(voltage0, "%sVoltage: ", focus0);
    sprintf(voltage1, "%s%s V", focus1, voltageV);
    char currentP0[18];
    char currentP1[18];
    sprintf(currentP0, "%sCurrent \"+\": ", focus0);
    sprintf(currentP1, "%s%s A", focus1, currentPV);
    char currentN0[18];
    char currentN1[18];
    sprintf(currentN0, "%sCurrent \"-\": ", focus0);
    sprintf(currentN1, "%s%s A", focus1, currentNV);
    char memory0[18];
    char memory1[18];
    sprintf(memory0, "%sFree RAM: ", focus0);
    sprintf(memory1, "%s%d byte", focus1, freeMemory());

    
    char* menu[10][3] = {
        {volume0, volume1},
        {bass0, bass1},
        {treble0, treble1},
        {source0, source1},
        {mode0, mode1},
        {voltage0, voltage1},
        {currentP0, currentP1},
        {currentN0, currentN1},
        {memory0, memory1},
    };

    if (millis() - _dynamicScreenTimer > 1000 || _updteDisplay == 1) {
        _dynamicScreenTimer = millis();
        char empty0[18 - strlen(menu[_menuPointer][0])] = "";
        char empty1[18 - strlen(menu[_menuPointer][1])] = "";
        memset(empty0, ' ', sizeof(empty0) - 1);
        memset(empty1, ' ', sizeof(empty1) - 1);
        empty0[sizeof(empty0) - 1] = '\0';
        empty1[sizeof(empty1) - 1] = '\0';
        char line0[18];
        char line1[18];
    
        strncpy(line0, menu[_menuPointer][0], sizeof(line0));
        strncat(line0, empty0, sizeof(line0));
        
        strncpy(line1, menu[_menuPointer][1], sizeof(line1));
        strncat(line1, empty1, sizeof(line1));
        _lcd->setCursor(0, 0);
        _lcd->print(line0);
        _lcd->setCursor(0, 1);
        _lcd->print(line1);
        _updteDisplay = 0;
    } 
    
    
    if (_encoder.isRight()) {
        if (_menuMode == 1) {
            _menuTimeOut = millis();
            _prevMenu();
        } else {
            _minus();
        }

        _updteDisplay = 1;
    }

    if (_encoder.isLeft()) {
        if (_menuMode == 1) {
            _menuTimeOut = millis();
            _nextMenu();
        } else {
            _plus();
        }

        _updteDisplay = 1;
    }

    if(_encoder.isHolded()) {
        _menuTimeOut = millis();
        _menuMode = 1;
        _updteDisplay = 1;
    }

    

    if(_encoder.isClick()) {
        if (_menuMode == 1) {
            _menuMode = 0;
            _menuTimeOut = millis();
        } else {
            _ac->switchMute();
        }
        
        _updteDisplay = 1;
    }

    if (_menuTimeOut > 0 && (millis() - _menuTimeOut) > 10000) {    
        if (_menuMode == 1) {
            _menuMode = 0;
        }
        
        _menuTimeOut = 0;
        _dynamicScreenTimer = 0;
        _menuPointer = 0;
        _updteDisplay = 1;
    }
}
void LiquidLiteMenu::_nextMenu() {
    if (_menuPointer == _menuSize - 2) {
        _menuPointer = 0;
    } else {
        _menuPointer = _menuPointer + 1;
    }

    _dynamicScreenTimer = millis();
}
void LiquidLiteMenu::_prevMenu() {    
    if (_menuPointer == 0) {
        _menuPointer = _menuSize - 2;
    } else {
        _menuPointer = _menuPointer - 1;
    }

    _dynamicScreenTimer = millis();
}
void LiquidLiteMenu::_plus() {
    if (_menuPointer == 0) {
        _ac->plusVolume();
    }

    if (_menuPointer == 1) {
        _ac->plusBass();
    }

    if (_menuPointer == 2) {
        _ac->plusTreb();
    }

    if (_menuPointer == 3) {
        _ac->switchSource();
    }

    if (_menuPointer == 4) {
        _ac->switchMode();
    }
};
void LiquidLiteMenu::_minus() {
    if (_menuPointer == 0) {
        _ac->minusVolume();
    }

    if (_menuPointer == 1) {
        _ac->minusBass();
    }

    if (_menuPointer == 2) {
        _ac->minusTreb();
    }

    if (_menuPointer == 3) {
        _ac->switchSource();
    }

    if (_menuPointer == 4) {
        _ac->switchMode();
    }
};
