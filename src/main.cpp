#include "Arduino.h"
#include "AT.h"
#include "Power.h"
#include "AmplifireControll.h"
#include "LiquidCrystal_I2C.h"
#include "GyverEncoder.h"
#include "LiquidLiteMenu.h"
#include "IRremote.h"

//Analog
byte voltageSensor = 0;
byte currentPositiveSensor = 1;
byte currentNegativeSensor = 2;

//Digital
byte powerGood = 2;
byte enabled12v = 13;
byte button1 = 3;
byte button2 = 4;
byte button3 = 5;
byte powerLed = 6;
byte switchMode1 = 7;
byte switchMode2 = 12;
byte irData = 8;

byte encoderSWITCH = 9;
byte encoderDT = 10;
byte encoderCLK = 11;

AT at;
Power power;
AmplifireControll amp;
LiquidCrystal_I2C lcd(0x3F,16,2);
LiquidLiteMenu menu;
boolean initedPower = false;
IRrecv irrecv(irData);
decode_results results;

void setup() {
  Serial.begin(9600);
  irrecv.enableIRIn();
  lcd.init(); 
  lcd.clear();
  pinMode(powerGood, OUTPUT);
  pinMode(button1, INPUT);
  pinMode(button2, INPUT);
  pinMode(button3, INPUT);
  pinMode(powerLed, OUTPUT);
  pinMode(switchMode1, OUTPUT);
  pinMode(switchMode2, OUTPUT);
  pinMode(enabled12v, OUTPUT);
  pinMode(irData, INPUT);
  pinMode(encoderSWITCH, INPUT);
  pinMode(encoderDT, INPUT);
  pinMode(encoderCLK, INPUT);

  power.init(voltageSensor, currentPositiveSensor, currentNegativeSensor, button1, powerLed, powerGood, switchMode1, switchMode2, enabled12v, &lcd);
  at.init(&power);
}
 
void loop() {
  power.tick();

  if (irrecv.decode(&results)) {
    Serial.print("0x");
    Serial.println(results.value, HEX);
    delay(500);
    irrecv.resume();// Receive the next value
  }
  
  if (power.systenBusy) {
    initedPower = false;
    return;
  }

  if (!initedPower) {
    delay(100);
    amp.init(switchMode1, switchMode2);
    menu.init(button2, button3, switchMode1, switchMode2, irData, encoderSWITCH, encoderDT, encoderCLK, &lcd, &amp, &power);
    initedPower = true;
  }

  at.commandsListener();
  amp.tick();
  menu.tick();
}


