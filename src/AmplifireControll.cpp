#include "AmplifireControll.h"
#include "Arduino.h"
#include "TDA8425.h"
#include "EEPROM.h"

// EPROM addresses
#define SOURCE 0
#define MODE 1
#define VOLUME 2
#define MUTE 3
#define BALANCE 4
#define BASS 5
#define TREBLE 6

// max and min values
#define VOLUME_MIN 27 // -76 dB
#define VOLUME_MAX 63 // +6 dB
#define BALANCE_MIN -4 // -4 dB
#define BALANCE_MAX 4 // +4 dB
#define BASS_MIN 2 // -12 dB
#define BASS_MAX 11 // +15 dB
#define TREB_MIN 2 // -12 dB
#define TREB_MAX 10 // +12 dB
#define SOURCE_MIN 0 // source "A"
#define SOURCE_MAX 1 // source "B"
#define MODE_MIN 0 // bridge off
#define MODE_MAX 1 // bridge on
#define MUTE_MIN 0 // mute off
#define MUTE_MAX 1 // mute on

AmplifireControll::AmplifireControll() {  
}

void AmplifireControll::init(byte switchMode1, byte switchMode2) {
    TDA8425 _tda;
    _switchMode1 = switchMode1;
    _switchMode2 = switchMode2;
    _timer = 0;

    uint8_t source = EEPROM.read(SOURCE);
    _setSource(source);

    uint8_t mode = EEPROM.read(MODE);
    _setMode(mode);

    uint8_t mute = EEPROM.read(MUTE);
    _setMute(mute);

    uint8_t volume = EEPROM.read(VOLUME);
    _setVolume(volume);
    
    uint8_t bass = EEPROM.read(BASS);
    _setBass(bass);
    
    uint8_t treb = EEPROM.read(TREBLE);
    _setTreb(treb);  
}


byte AmplifireControll::_setVolume (uint8_t value) {
    if  (value > VOLUME_MAX) {
        _tda.setVolume(VOLUME_MAX);
        return VOLUME_MAX;
    }
    
    if (value < VOLUME_MIN) {
        _tda.setVolume(VOLUME_MIN);
        return VOLUME_MIN;
    }
    
    _tda.setVolume(value);
    return value;
}

byte AmplifireControll::_setBass (uint8_t value) {
    if  (value > BASS_MAX) {
        _tda.setBass(BASS_MAX);
        return BASS_MAX;
    } 
    
    if (value < BASS_MIN) {
        _tda.setBass(BASS_MIN);
        return BASS_MIN;
    }

    _tda.setBass(value);
    return value;
}

byte AmplifireControll::_setTreb (uint8_t value) {
    if  (value > TREB_MAX) {
        _tda.setTreble(TREB_MAX);
        return TREB_MAX;
    }
    
    if (value < TREB_MIN) {
        _tda.setTreble(TREB_MIN);
        return TREB_MIN;
    }
    
    _tda.setTreble(value);
    return value;
}

byte AmplifireControll::_setSource (uint8_t value) {
    if  (value > SOURCE_MAX) {
        _tda.setSource(SOURCE_MAX);
        return SOURCE_MAX;
    }
    
    if (value < SOURCE_MIN) {
        _tda.setSource(SOURCE_MIN);
        return SOURCE_MIN;
    }
    
    _tda.setSource(value);
    return value;
}

byte AmplifireControll::_setMute (uint8_t value) {
    if  (value > MUTE_MAX) {
        _tda.setMute(MUTE_MAX);
        return MUTE_MAX;
    }
    
    if (value < MUTE_MIN) {
        _tda.setMute(MUTE_MIN);
        return MUTE_MIN;
    }
    
    _tda.setMute(value);
    return value;
}

byte AmplifireControll::_setMode (uint8_t value) {
    if  (value > MODE_MAX) {
        _tda.setMute(MODE_MAX);
        digitalWrite(_switchMode1, MODE_MAX);
        digitalWrite(_switchMode2, MODE_MAX);
        _mode = MODE_MAX;
        return MODE_MAX;
    }
    
    if (value < MODE_MIN) {
        digitalWrite(_switchMode1, MODE_MIN);
        digitalWrite(_switchMode2, MODE_MIN);
        _mode = MODE_MIN;
        return MODE_MIN;
    }
    
    digitalWrite(_switchMode1, value);
    digitalWrite(_switchMode2, value);
    _mode = value;
    return value;
}

//Controll functions
ampData AmplifireControll::getVolumeData() {
    boolean mute = _tda.getMute() == 1;
    byte volume = _tda.getVolumeL();
    ampData data;
    data.percentVal = mute ? 0 : map(volume, 27, 63, 0, 100);
    data.humanVal = mute ? -76 : map(volume, 27, 63, -76, 16);
    data.humanUnit = "dB";
    return data;
}

ampData AmplifireControll::plusVolume() {
    _setVolume(_tda.getVolumeL() + 1);
    _setMute(0);
    return getVolumeData();
};

ampData AmplifireControll::minusVolume() {
    _setVolume(_tda.getVolumeL() - 1);
    _setMute(0);
    return getVolumeData();
};

ampData AmplifireControll::getBassData() {
    byte bass = _tda.getBass();
    ampData data;
    data.percentVal = map(bass, 2, 11, 0, 100);
    data.humanVal = map(bass, 2, 11, -12, 15);
    data.humanUnit = "dB";
    return data;
}

ampData AmplifireControll::plusBass() {
    _setBass(_tda.getBass() + 1);
    return getBassData();
};

ampData AmplifireControll::minusBass() {
    _setBass(_tda.getBass() - 1);
    return getBassData();
};

ampData AmplifireControll::getTrebData() {
    byte treb = _tda.getTreble();
    ampData data;
    data.percentVal = map(treb, 2, 10, 0, 100);
    data.humanVal = map(treb, 2, 10, -12, 15);
    data.humanUnit = "dB";
    return data;
}

ampData AmplifireControll::plusTreb() {
    _setTreb(_tda.getTreble() + 1);
    return getTrebData();
};

ampData AmplifireControll::minusTreb() {
    _setTreb(_tda.getTreble() - 1);
    return getTrebData();
};

ampData AmplifireControll::getSourceData() {
    byte source = _tda.getSource();
    ampData data;
    data.percentVal = source == 1 ? 0 : 100;
    data.humanVal = source == 0 ? 1 : 2;
    data.humanUnit = "channel";
    return data;
}

ampData AmplifireControll::switchSource() {
    _setSource(_tda.getSource() == 1 ? 0 : 1);
    return getSourceData();
};

ampData AmplifireControll::getMuteData() {
    byte muteStatus = _tda.getMute();
    ampData data;
    data.percentVal = muteStatus == 1 ? 0 : 100;
    data.humanVal = muteStatus == 0 ? 1 : 2;
    data.humanUnit = "mute status";
    return data;
}

ampData AmplifireControll::switchMute() {
    _setMute(_tda.getMute() == 1 ? 0 : 1);
    return getMuteData();
};

ampData AmplifireControll::getModeData() {
    byte mode = _mode;
    ampData data;
    data.percentVal = mode == 1 ? 0 : 100;
    data.humanVal = mode == 0 ? 1 : 2;
    data.humanUnit = "bridge mode";
    return data;
}

ampData AmplifireControll::switchMode() {
    _setMode(_mode == 1 ? 0 : 1);
    return getModeData();
};

void AmplifireControll::tick() {
    if (millis() - _timer > 1000 ) {
        _timer = millis();
        byte source = _tda.getSource();
        byte mode = _mode;
        byte volume = _tda.getVolumeL();
        byte mute = _tda.getMute();
        byte bass = _tda.getBass();
        byte treb = _tda.getTreble();
        
        if (EEPROM.read(SOURCE) != source) {
            EEPROM.update(SOURCE, source);
        }
        
        if (EEPROM.read(MODE) != mode) {
            EEPROM.update(MODE, mode);
        }
        
        if (EEPROM.read(VOLUME) != volume) {
            EEPROM.update(VOLUME, volume);
        }
        
        if (EEPROM.read(MUTE) != mute) {
            EEPROM.update(MUTE, mute);
        }
        
        if (EEPROM.read(BASS) != bass) {
            EEPROM.update(BASS, bass);
        }
        
        if (EEPROM.read(TREBLE) != treb) {
            EEPROM.update(TREBLE, treb);
        }
    }
}