#include "SystemManager.h"
#include "Power.h"
#include "LiquidCrystal_I2C.h"
#include "GyverEncoder.h"
#include "TDA8425.h"
#include "EEPROM.h"
#include "LiquidMenu.h"

#define SOURCE 0
#define MODE 1
#define VOLUME 2
#define MUTE 3
#define BALANCE 4
#define BASS 5
#define TREBLE 6

SystemManager::SystemManager() {}
uint8_t lFocus[8] = {
	0b00100,
	0b00110,
	0b00101,
	0b00100,
	0b00100,
	0b01100,
	0b10100,
	0b11000
};

byte SystemManager::CURRENT_CB = 0;

void SystemManager::cbVolume() {
    SystemManager::CURRENT_CB = 0;   
    return;
}

void SystemManager::cbBalance() {
    SystemManager::CURRENT_CB = 1;   
    return;
}

void SystemManager::cbBass() {
    SystemManager::CURRENT_CB = 2;   
    return;
}

void SystemManager::cbTreb() {
    SystemManager::CURRENT_CB = 3;   
    return;
}

void SystemManager::cbSource() {
    SystemManager::CURRENT_CB = 4;   
    return;
}

void SystemManager::cbMode() {
    SystemManager::CURRENT_CB = 5;   
    return;
}

void SystemManager::cbMute() {
    SystemManager::CURRENT_CB = 6;   
    return;
}

void SystemManager::init(byte button2, byte button3, byte switchMode1, byte switchMode2, byte irData, byte encoderSWITCH, byte encoderDT, byte encoderCLK, LiquidCrystal_I2C* lcd, Power* power) {
    Encoder encoder(encoderCLK, encoderDT, encoderSWITCH, TYPE2);
    encoder.setTickMode(AUTO);
    TDA8425 tda;
    _menuMode = false;
    _encoder = encoder;
    _tda = tda;
    _power = power;
    _lcd = lcd;
    _button2 = button2;
    _button3 = button3;
    _switchMode1 = switchMode1;
    _switchMode2 = switchMode2;
    _irData = irData;
    _menuPosition = 0;
    _sourceMin = 0;
    _sourceMax = 1;
    _modeMin = 0;
    _modeMax = 1;
    _muteMin = 0;
    _muteMax = 1;
    _volumeMin = 27; // -76 dB
    _volumeMax = 63; // +6 dB
    _balanceMin = -4; // -4 dB
    _balanceMax = 4; // +4 dB
    _bassMin = 2; // -12 dB
    _bassMax = 11; // +15 dB
    _trebMin = 2; // -12 dB
    _trebMax = 10; // +12 dB

    uint8_t source = EEPROM.read(SOURCE);
    _source = (source > _sourceMax || source < _sourceMin) ? 0 : source;
    
    uint8_t mode = EEPROM.read(MODE);
    _mode = (mode > _modeMax || source < _modeMin) ? 0 : mode;
    
    uint8_t mute = EEPROM.read(MUTE);
    _mute = (mute > _muteMax || mute < _muteMin) ? 0 : mute;
    
    uint8_t volume = EEPROM.read(VOLUME);
    _volume = (volume > _volumeMax || volume < _volumeMin) ? _volumeMin : volume;
    _hVolume = map(_volume, 27, 63, -76, 16);
    _volumePercent = map(_volume, 27, 63, 0, 100);
    
    int8_t balance = EEPROM.read(BALANCE);
    _balance = (balance > _balanceMax || balance < _balanceMin) ? 0 : balance;
    
    uint8_t bass = EEPROM.read(BASS);
    _bass = (bass > _bassMax || bass < _bassMin) ? _bassMin : bass;
    _hBass = map(_bass, 2, 11, -12, 15);
    _bassPercent = map(_bass, 2, 11, 0, 100);
    
    uint8_t treb = EEPROM.read(TREBLE);
    _treb = (treb > _trebMax || treb < _trebMin) ? _trebMin : treb;
    _hTreb = map(_treb, 2, 10, -12, 15);
    _trebPercent = map(_treb, 2, 10, 0, 100);

    _tda.setSource(_source);
    _tda.setVolume(_volume);
    _tda.setBass(_bass);
    _tda.setTreble(_treb);
    _tda.setMute(_mute);
    
    digitalWrite(_switchMode1, _mode);
    digitalWrite(_switchMode2, _mode);

    _menu = new LiquidMenu(*_lcd);
    _menu->init();
    
    LiquidLine volumeLine1(0, 0, "Volume: ");
    volumeLine1.set_focusPosition(Position::CUSTOM, 15, 0);
    volumeLine1.attach_function(1, SystemManager::cbVolume);
    LiquidLine volumeLine2(0, 1, _hVolume, " dB (", _volumePercent, "%)  ");
    volumeLine2.set_focusPosition(Position::CUSTOM, 15, 1);
    volumeLine2.attach_function(2, SystemManager::cbVolume);
    LiquidScreen volumeScreen(volumeLine1, volumeLine2);

    // LiquidLine bassLine1(0, 0, "Bass: ");
    // bassLine1.set_focusPosition(Position::CUSTOM, 15, 0);
    // bassLine1.attach_function(1, SystemManager::cbBass);
    // LiquidLine bassLine2(0, 1, _hBass, " dB (", _bassPercent, "%)  ");
    // bassLine2.set_focusPosition(Position::CUSTOM, 15, 1);
    // bassLine2.attach_function(2, SystemManager::cbBass);
    // LiquidScreen bassScreen(bassLine1, bassLine2);

    LiquidLine trebLine1(0, 0, "Treble: ");
    trebLine1.set_focusPosition(Position::CUSTOM, 15, 0);
    trebLine1.attach_function(1, SystemManager::cbTreb);
    LiquidLine trebLine2(0, 1, _hTreb, " dB (", _trebPercent, "%)  ");
    trebLine2.set_focusPosition(Position::CUSTOM, 15, 1);
    trebLine2.attach_function(2, SystemManager::cbTreb);
    LiquidScreen trebScreen(trebLine1, trebLine2);
    
    _menu->add_screen(volumeScreen);
    // _menu->add_screen(bassScreen);
    _menu->add_screen(trebScreen);
    _menu->switch_focus(false);
    _menu->set_focusSymbol(Position::CUSTOM, lFocus);
    _menu->update();
}


void SystemManager::systemListiner() {    
    if (_encoder.isPress()) {
        if (_menuMode) {
            _menu->switch_focus(true);
            _menu->call_function(2);
            _menuMode = false;
        } else {
            // switchMode();
        }
    }
    
    
    if (_encoder.isRight()) {
        if (_menuMode) {
            _menuTimeOut = millis();
            _menu->previous_screen();
            _menu->set_focusedLine(0);
            _menu->softUpdate();
            
        } else {
            switch (SystemManager::CURRENT_CB) {
                case 0:
                default:
                    minusVolume();
                    break;
                case 1:
                    break;
                case 2:
                    minusBass();
                    break;
                case 3:
                    minusTreb();
                    break;
            }
            
        }
    }

    if (_encoder.isLeft()) {
        if (_menuMode) {
            _menuTimeOut = millis();
            _menu->next_screen();
            _menu->set_focusedLine(0);
            _menu->softUpdate();
        } else {
            switch (SystemManager::CURRENT_CB) {
                case 0:
                default:
                    plusVolume();
                    break;
                case 1:
                    break;
                case 2:
                    plusBass();
                    break;
                case 3:
                    plusTreb();
                    break;
            }
        }
    }


    if (_encoder.isHolded()) {
        _menuTimeOut = millis();
        _menuMode = true;
        _menu->switch_focus(false);
        _menu->softUpdate();
    }

    if (millis() - _menuTimeOut > 10000) {
        if (_menuMode) {
            _menu->change_screen(1);
            _menu->call_function(2);
            _menu->switch_focus(true);
            _menu->softUpdate();
        }
        _menuMode = false;
    }
    
    
    if (millis() - _timer > 1000 ) {
        _timer = millis();
        
        if (EEPROM.read(SOURCE) != _source) {
            EEPROM.update(SOURCE, _source);
        }
        
        if (EEPROM.read(MODE) != _mode) {
            EEPROM.update(MODE, _mode);
        }
        
        if (EEPROM.read(VOLUME) != _volume) {
            EEPROM.update(VOLUME, _volume);
        }
        
        if (EEPROM.read(MUTE) != _mute) {
            EEPROM.update(MUTE, _mute);
        }
        
        if (EEPROM.read(BALANCE) != _balance) {
            EEPROM.update(BALANCE, _balance);
        }
        
        if (EEPROM.read(BASS) != _bass) {
            EEPROM.update(BASS, _bass);
        }
        
        if (EEPROM.read(TREBLE) != _treb) {
            EEPROM.update(TREBLE, _treb);
        }
    }
}

//Controll functions
void SystemManager::plusVolume() {
    _volume = _volume + 1 <= _volumeMax ? _volume + 1 : _volume;
    _tda.setVolume(_volume);
    _hVolume = map(_volume, 27, 63, -76, 16);
    _volumePercent = map(_volume, 27, 63, 0, 100);
    _menu->softUpdate();
};

void SystemManager::minusVolume() {
    _volume = _volume - 1 >= _volumeMin ? _volume - 1 : _volume;
    _tda.setVolume(_volume);
    _hVolume = map(_volume, 27, 63, -76, 16);
    _volumePercent = map(_volume, 27, 63, 0, 100);
    _menu->softUpdate();
};

void SystemManager::plusBalans() {
    // _balance = _balance + 1 <= _balanceMax ? _balance + 1 : _balance;
    // _tda.setVolume(_balance);
};

void SystemManager::minusBalans() {
    // _balance = _balance - 1 >= _balanceMin ? _balance - 1 : _balance;
    // _tda.setVolume(_balance);
};

void SystemManager::plusBass() {
    _bass = _bass + 1 <= _bassMax ? _bass + 1 : _bass;
    _tda.setBass(_bass);
    _hBass = map(_bass, 2, 11, -12, 15);
    _bassPercent = map(_bass, 2, 11, 0, 100);
    _menu->softUpdate();
};

void SystemManager::minusBass() {
    _bass = _bass - 1 >= _bassMin ? _bass - 1 : _bass;
    _tda.setBass(_bass);
    _hBass = map(_bass, 2, 11, -12, 15);
    _bassPercent = map(_bass, 2, 11, 0, 100);
    _menu->softUpdate();
};

void SystemManager::plusTreb() {
    _treb = _treb + 1 <= _trebMax ? _treb + 1 : _treb;
    _hTreb = map(_treb, 2, 10, -12, 15);
    _trebPercent = map(_treb, 2, 10, 0, 100);
    _tda.setTreble(_treb);
};

void SystemManager::minusTreb() {
    _treb = _treb - 1 >= _trebMin ? _treb - 1 : _treb;
    _hTreb = map(_treb, 2, 10, -12, 15);
    _trebPercent = map(_treb, 2, 10, 0, 100);
    _tda.setTreble(_treb);
};

void SystemManager::switchSource() {
    _source = _source == 0 ? 1 : 0;
    _tda.setSource(_source);
};

void SystemManager::switchMute() {
    _mute = _mute == 0 ? 1 : 0;
    _tda.setMute(_mute);
};

void SystemManager::switchMode() {
    _mode = _mode == 0 ? 1 : 0;
    digitalWrite(_switchMode1, _mode);
    digitalWrite(_switchMode2, _mode);
};
