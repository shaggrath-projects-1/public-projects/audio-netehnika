#include "Power.h"
#include "LiquidCrystal_I2C.h"


#define MAX_CURRENT 15.00
#define MAX_VOLATGE 30.00
#define RETRY_DELAY 500

Power::Power() {};

void Power::init(byte voltageSensor, byte currentPositiveSensor, byte currentNegativeSensor, byte powerButton, byte powerLed, byte powerGood, byte switchMode1, byte switchMode2, byte enabled12v, LiquidCrystal_I2C* lcd) {
    _lcd = lcd;
    _voltageSensor = voltageSensor;
    _currentPositiveSensor = currentPositiveSensor;
    _currentNegativeSensor = currentNegativeSensor;
    _powerButton = powerButton;
    _powerLed = powerLed;
    _powerGood = powerGood;
    _switchMode1 = switchMode1;
    // _switchMode2 = switchMode2;
    _enabled12v = enabled12v;
    _previusButtonStatus = digitalRead(_powerButton);

    _lcd->noBacklight();
    digitalWrite(_powerLed, false);
    digitalWrite(_powerGood, false);
    systenBusy = true;
} 

float Power::_mapFloat(int16_t x, int16_t in_min, int16_t in_max, int16_t out_min, int16_t out_max) {
    return (float)(x - in_min) * (out_max - out_min) / (float)(in_max - in_min) + out_min;
}

void Power::tick() {
    boolean powerButtonStatus = digitalRead(_powerButton);

    if (powerButtonStatus != _previusButtonStatus) {
        
        boolean firstTest = powerButtonStatus;
        delay(150);
        boolean nextTest = digitalRead(_powerButton);

        if (firstTest != nextTest) {
            return;
        }

        if (_powerOn) {
            Power::turnOff("Bye Bye...");
        } else {
            Power::turnOn();
        }
    }

    if (Power::getVoltageValue() > MAX_VOLATGE) {
        delay(RETRY_DELAY);
        
        if (Power::getVoltageValue() > MAX_VOLATGE) {
            Power::turnOff("hight voltage!");
        }
    }

    if (Power::getCurrentPositiveValue() > MAX_CURRENT) {
        delay(RETRY_DELAY);

        if (Power::getCurrentPositiveValue() > MAX_CURRENT) {
            Power::turnOff("hight current! +");
        }
    }

    if (Power::getCurrentNegativeValue() > MAX_CURRENT) {
        delay(RETRY_DELAY);

        if (Power::getCurrentNegativeValue() > MAX_CURRENT) {
            Power::turnOff("hight current! -");
        }
    }
}

double Power::getCurrentPositiveValue() {
    int16_t sensorValue = analogRead(_currentPositiveSensor);
    return _mapFloat(sensorValue, 0, 1023, -20, 20);
}

double Power::getCurrentNegativeValue() {
    int16_t sensorValue = analogRead(_currentNegativeSensor);
    return _mapFloat(sensorValue, 0, 1023, -20, 20);
}


double Power::getVoltageValue() {
    int16_t sensorValue = analogRead(_voltageSensor);
    return _mapFloat(sensorValue, 0, 1023, 0, 56);
}



void Power::turnOn() {
    _lcd->clear();
    _lcd->backlight();
    _lcd->setCursor(0, 0);
    _lcd->print("AUDIO neTEHNIKA");
    _lcd->setCursor(0, 1);
    _lcd->print("version 1.0.0");
    
    systenBusy = true;
    digitalWrite(_switchMode2, false);

    if (Power::getCurrentPositiveValue() < MAX_CURRENT && Power::getVoltageValue() < MAX_VOLATGE) {
        delay(2000);

        if (Power::getCurrentPositiveValue() < MAX_CURRENT && Power::getVoltageValue() < MAX_VOLATGE) {
            digitalWrite(_enabled12v, true);
            delay(500);
            digitalWrite(_switchMode1, true);
            delay(500);
            digitalWrite(_powerLed, true);    
            digitalWrite(_powerGood, true);
            delay(500);
            digitalWrite(_switchMode1, false);

            _powerOn = true;
            systenBusy = false;
            _lcd->clear();
        }
        
    }    
}

void Power::turnOff(String message) {
    systenBusy = true;
    _lcd->clear();
    _lcd->setCursor(0, 0);
    _lcd->print(message);
    digitalWrite(_switchMode2, false);
    digitalWrite(_switchMode1, true);
    delay(500);
    digitalWrite(_powerLed, false);
    digitalWrite(_powerGood, false);
    delay(2000);
    digitalWrite(_enabled12v, false);
    digitalWrite(_switchMode1, false);
    _lcd->clear();
    _lcd->noBacklight();
    _powerOn = false;
}