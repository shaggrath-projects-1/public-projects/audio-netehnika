#include "AT.h"
#include "Power.h"

AT::AT() {};

void AT::init(Power* power) {
    _command = "";
    _power = power;
    Serial.println("AT inited OK!");
}

void AT::commandsListener() {
    if (Serial.available()) {
        _terminalCahr = Serial.read();
        if (_terminalCahr != '\n') {
            _command = _command + _terminalCahr;
            return;
        }
        commandRunner(_command);
        
        _command = "";
    };
};

void AT::commandRunner(String command) {
    if (command.equals(_AT_NAME)) {
        Serial.println("amplifier");
        return;
    }

    if (command.equals(_AT_VERSION)) {
        Serial.println("0.0.0");
        return;
    }

    if (command.equals(_AT_VOLTAGE)) {
        Serial.println("voltage: " + (String)_power->getVoltageValue() + " v");
        return;
    }

    if (command.equals(_AT_CURRENT)) {
        Serial.println("positive current: " + (String)_power->getCurrentPositiveValue() + "A");
        Serial.println("negative current: " + (String)_power->getCurrentNegativeValue() + "A");
        return;
    }

    Serial.print("'");
    Serial.print(command);
    Serial.println("' unknow command!");
}
