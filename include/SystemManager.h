/*
  SystemManager.h - Library for controll aplifire system
  Created by Steven Reed, 2020.
  Released into the public domain.
*/
#ifndef SYSTEM_MANAGER_h
#define SYSTEM_MANAGER_h

#include "Arduino.h"
#include "LiquidCrystal_I2C.h"
#include "Power.h"
#include "GyverEncoder.h"
#include "TDA8425.h"
#include "LiquidMenu.h"


class SystemManager {
  public:
    SystemManager();
    void init(byte button2, byte button3, byte switchMode1, byte switchMode2, byte irData, byte encoderSWITCH, byte encoderDT, byte encoderCLK, LiquidCrystal_I2C* lcd, Power* power);
    void systemListiner();
    void plusVolume();
    void minusVolume();
    void plusBalans();
    void minusBalans();
    void plusBass();
    void minusBass();
    void plusTreb();
    void minusTreb();
    void switchSource();
    void switchMute();
    void switchMode(); //bridge
    static void cbVolume(); // 0
    static void cbBalance(); // 1
    static void cbBass(); // 2
    static void cbTreb(); // 3
    static void cbSource(); // 4
    static void cbMode(); // 5
    static void cbMute(); // 6
    static byte CURRENT_CB;
  private:
    Power* _power;
    LiquidCrystal_I2C* _lcd;
    Encoder _encoder;
    TDA8425 _tda;
    LiquidMenu* _menu;
    byte _button2;
    byte _button3;
    byte _switchMode1;
    byte _switchMode2;
    byte _irData;
    boolean _menuMode;
    uint16_t _menuTimeOut;
    uint8_t _menuPosition;
    uint8_t _source;
    uint8_t _mode;
    uint8_t _volume;
    int8_t _volumePercent;
    int8_t _hVolume;
    uint8_t _mute;
    int8_t _balance;
    uint8_t _bass;
    int8_t _bassPercent;
    int8_t _hBass;
    uint8_t _treb;
    int8_t _trebPercent;
    int8_t _hTreb;
    uint8_t _modeMin;
    uint8_t _modeMax;
    uint8_t _sourceMin;
    uint8_t _sourceMax;
    uint8_t _volumeMin;
    uint8_t _volumeMax;
    uint8_t _muteMin;
    uint8_t _muteMax;
    int8_t _balanceMin;
    int8_t _balanceMax;
    uint8_t _bassMin;
    uint8_t _bassMax;
    uint8_t _trebMin;
    uint8_t _trebMax;
    uint16_t _timer;
    char* _volFirstLine;
    char* _volLasttLine;
};

#endif
