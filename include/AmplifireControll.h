#ifndef AmplifireControll_h
#define AmplifireControll_h

#include "Arduino.h"
#include "TDA8425.h"

struct ampData {
  int8_t percentVal;
  int8_t humanVal;
  String humanUnit;
};

class AmplifireControll {
    public:
        AmplifireControll();
        void init(byte switchMode1, byte switchMode2);
        ampData plusVolume();
        ampData minusVolume();
        ampData plusBalans();
        ampData minusBalans();
        ampData plusBass();
        ampData minusBass();
        ampData plusTreb();
        ampData minusTreb();
        ampData switchSource();
        ampData switchMute();
        ampData switchMode();
        ampData getVolumeData();
        ampData getBalansData();
        ampData getBassData();
        ampData getTrebData();
        ampData getSourceData();
        ampData getMuteData();
        ampData getModeData();
        void tick();
    private:
        TDA8425 _tda;
        byte _switchMode1;
        byte _switchMode2;
        uint8_t _mode;
        long _timer;
        byte _setVolume(uint8_t value);
        byte _setBalans(int8_t value);
        byte _setBass(uint8_t value);
        byte _setTreb(uint8_t value);
        byte _setSource(uint8_t value);
        byte _setMute(uint8_t value);
        byte _setMode(uint8_t value); //bridge
};

#endif
