/*
  LiquidLiteMenu.h - Library for organization menu on liquid display 16*2
  Created by Steven Reed, 2020.
  Released into the public domain.
*/
#ifndef LIQUID_LITE_MENU
#define LIQUID_LITE_MENU

#include "Arduino.h"
#include "LiquidCrystal_I2C.h"
#include "AmplifireControll.h"
#include "GyverEncoder.h"
#include "Power.h"

class LiquidLiteMenu {
    public:
        LiquidLiteMenu();
        void init(byte button2, byte button3, byte switchMode1, byte switchMode2, byte irData, byte encoderSW, byte encoderDT, byte encoderCLK, LiquidCrystal_I2C* lcd, AmplifireControll* ac, Power* power);
        void tick();
    private:
        LiquidCrystal_I2C* _lcd;
        AmplifireControll* _ac;
        Power* _power;
        Encoder _encoder;
        byte _button2;
        byte _button3;
        byte _switchMode1;
        byte _switchMode2;
        byte _irData;
        byte _encoderSW;
        byte _encoderDT;
        byte _encoderCLK;
        byte _menuSize;
        byte _menuPointer;
        byte _menuMode; //0 - local || 1 - global
        byte _updteDisplay;
        byte _dynamicScreen;
        long _dynamicScreenTimer;
        long _menuTimeOut;
        void _nextMenu();
        void _prevMenu();
        void _plus();
        void _minus();
};

#endif