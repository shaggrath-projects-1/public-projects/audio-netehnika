/*
  AT.h - Library for read AT commands from serial port
  Created by Steven Reed, 2019.
  Released into the public domain.
*/
#ifndef AT_h
#define AT_h

#include "Arduino.h"
#include "Power.h"

class AT {
    public:
        AT();
        void init(Power* power);
        void commandsListener();
        
    private:
        String _command;
        String _AT_NAME = "AT+NAME";
        String _AT_VERSION = "AT+VERSION";
        String _AT_VOLTAGE = "AT+VOLTAGE";
        String _AT_CURRENT = "AT+CURRENT";
        char _terminalCahr;
        Power* _power;
        void commandRunner(String command);
};

#endif
