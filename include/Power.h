/*
  Power.h - Library for controll power on amplifire
  Created by Steven Reed, 2020.
  Released into the public domain.
*/
#ifndef POWER_h
#define POWER_h

#include "Arduino.h"
#include "LiquidCrystal_I2C.h"

class Power {
    public:
        Power();
        void init(byte voltageSensor, byte currentPositiveSensor, byte currentNegativeSensor, byte powerButton, byte powerLed, byte powerGood, byte switchMode1, byte switchMode2, byte enabled12v,  LiquidCrystal_I2C* lcd);
        void tick();
        double getCurrentPositiveValue();
        double getCurrentNegativeValue();
        double getVoltageValue();
        void turnOn();
        void turnOff(String message);
        boolean systenBusy;
    private:
        byte _voltageSensor;
        byte _currentPositiveSensor;
        byte _currentNegativeSensor;
        byte _powerButton;
        byte _powerLed;
        byte _powerGood;
        byte _switchMode1;
        byte _switchMode2;
        byte _enabled12v;
        boolean _previusButtonStatus = false;
        boolean _powerOn = false;
        LiquidCrystal_I2C* _lcd;
        float _mapFloat(int16_t x, int16_t in_min, int16_t in_max, int16_t out_min, int16_t out_max);
};

#endif
